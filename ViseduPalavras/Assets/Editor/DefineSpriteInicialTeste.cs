﻿using UnityEngine;
using UiImage = UnityEngine.UI.Image;
using System.Collections;
//using UnityEditor;

[ExecuteInEditMode]
public class DefineSpriteInicialTeste : MonoBehaviour {

	//#if UNITY_EDITOR
	void Start () {

		var img = GameObject.Find ("ImagemParaReconher").GetComponent<UiImage> ();
		SelecionaSpritesTeste sprite = new SelecionaSpritesTeste ();
		sprite.Start();
		img.sprite = sprite.getSpriteAleatoria();	

		//while(img.sprite == null){
		//	img.sprite = sprite.getSpriteAleatoria();
		//}
	}
	//#endif
}
