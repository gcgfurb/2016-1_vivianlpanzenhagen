﻿using System;
using UnityEngine;
using System.Collections;

public class SkinController : MonoBehaviour 
{
	public string activeSkin;
	
	// Use this for initialization
	void Start () 
	{
		// default loaded sprites
		SpriteRenderer[] loadedRenderers = GetComponentsInChildren <SpriteRenderer>(true);
		
		Texture2D atlas = Resources.Load (activeSkin, typeof(Texture2D)) as Texture2D;
		
		for (int i = 0; i < loadedRenderers.Length; i++)
		{
			try
			{
				// replace the current sprite with the desired sprite, but using the 
				// loaded sprite as a cut out reference via 'rect'
				loadedRenderers[i].sprite = 
					Sprite.Create (atlas, loadedRenderers[i].sprite.rect, new Vector2 (0.5f, 0.5f));
				
				// update name, main texture and shader, these all seem to be required... 
				// even thou you'd think it already has a shader :|
				loadedRenderers[i].sprite.name = loadedRenderers[i].name + "_sprite";
				loadedRenderers[i].material.mainTexture = atlas as Texture;
				loadedRenderers[i].material.shader = Shader.Find ("Assests/imagens");
			}
			catch (Exception e) {}
		}
	}
}
