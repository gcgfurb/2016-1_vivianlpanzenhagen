﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour {

	public Image preenchimento;
	public float velocidade = 60.0f;
	float pontos = 0;

	public void Start(){
		preenchimento.fillAmount = 0;
	}

	void Update (){
		preenchimento.fillAmount += 1.0f/velocidade * Time.deltaTime;
		pontos =(1 - preenchimento.fillAmount) * 100;
		PlayerPrefs.SetInt ("SalvaPonto",(int)pontos);

		if (preenchimento.fillAmount == 1) {
			Application.LoadLevel("notaFinal");	
		}
	}
}
