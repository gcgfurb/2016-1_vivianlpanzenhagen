﻿using UnityEngine;
using UiImage = UnityEngine.UI.Image;
using System.Collections;

public class OpcoesPause : MonoBehaviour {

	public void voltarJogo () {
		Time.timeScale = 1;
		Canvas canvasPause = GameObject.Find("CanvasPause").GetComponent<Canvas>();
		canvasPause.enabled = false;
	}


	public void sair(string nomeCena){
		Time.timeScale = 1;
		Application.LoadLevel (nomeCena);
	}
}
