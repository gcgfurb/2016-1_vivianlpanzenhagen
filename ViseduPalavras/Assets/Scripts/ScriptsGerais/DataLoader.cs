﻿using UnityEngine;
using System.Collections;

public class DataLoader : MonoBehaviour {
	public string[] items;
	private string url = "http://localhost/unityT/config.php";
	private string login = "root";
	private string senha = "";
	
	IEnumerator Start(){             
		WWW itemsData = new WWW(url + "?login=" + login + "&senha=" + senha);
		yield return itemsData;
		string itemsDataString = itemsData.text;
		print (itemsDataString);
		items = itemsDataString.Split(';');

	}
	
	string GetDataValue(string data, string index){
		string value = data.Substring(data.IndexOf(index)+index.Length);
		if(value.Contains("|"))value = value.Remove(value.IndexOf("|"));
		return value;
	}
	
	
}