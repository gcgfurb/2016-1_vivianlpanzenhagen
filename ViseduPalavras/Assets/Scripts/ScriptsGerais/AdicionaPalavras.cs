﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Threading; 
using System.IO;
using UiImage = UnityEngine.UI.Image;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AdicionaPalavras : MonoBehaviour {

	private int nivel;

	#if UNITY_EDITOR
	[MenuItem ("AssetDatabase/loadAllAssetsAtPath")]
	#endif

	void Start () {

		string myDir = "C:\\Users\\Vivian\\Documents";		
		DirectoryInfo dir = new DirectoryInfo (myDir);
		Sprite sp;

		List<System.IO.FileInfo> info = new List<System.IO.FileInfo> ();
		info.AddRange (dir.GetFiles ("*.png", System.IO.SearchOption.TopDirectoryOnly));
		info.AddRange (dir.GetFiles ("*.jpg", System.IO.SearchOption.TopDirectoryOnly));
		if (info != null) {
			string[] paths = new string[info.Count];
			for (int i = 0; i < info.Count; i++) {
				string nomeImagem = Path.GetFileNameWithoutExtension (paths [i]);
				if (nomeImagem.Contains ("F_")) {
					nivel = 1;
					string imagensFacil = paths [i];

					byte[] data = File.ReadAllBytes (paths [i]);
					Texture2D texture = new Texture2D (90, 90, TextureFormat.ARGB32, false);
					texture.LoadImage (data);

					texture.name = Path.GetFileNameWithoutExtension (paths [i]);
					sp = Sprite.Create (texture, new Rect (20.0f, 20.0f, 90.0f, 90.0f), new Vector2 (0.2f, 0.2f), 1000);
					sp.name = texture.name;
					Debug.Log ("Sprite " + sp.name);
					var img = GameObject.Find ("ImagemParaReconher").GetComponent<UiImage> ();
					img.sprite = sp;
				} else if (nomeImagem.Contains ("M_")) {
					nivel = 2;
				} else if (nomeImagem.Contains ("D_")) {
					nivel = 3;
				}
			}

		}
	}
	

}
