﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UiImage = UnityEngine.UI.Image;

public class ConfigCenaNivel : MonoBehaviour {

	public Button btnPlay;
	public Text txtNomeNivel;
	public string[] nomeNivel;
	private int idNivel;
	//private int help = 0;

	void Start () {
		idNivel = 0;
		txtNomeNivel.text = nomeNivel [idNivel];
		btnPlay.interactable = false;
	}

	public void selecioneNivel(int i){
		idNivel = i;
		txtNomeNivel.text = "Você escolheu o nível " +nomeNivel [i];
		PlayerPrefs.SetInt ("NivelEscolhido",idNivel);
		Debug.Log ("Niveeeeeeeeeeeeeeeeeeel"+idNivel);
		btnPlay.interactable = true;
	}

	public void ajuda(){
		//Time.timeScale = 0;
		var canvas = GameObject.Find ("CanvasHelp").GetComponent<Canvas> ();
		var btnHelp = GameObject.Find ("BtnHelp").GetComponent<Button> ();
		var btnSairHelp = GameObject.Find ("BtnSairHelp").GetComponent<Button> ();
		canvas.enabled = true;
		btnHelp.enabled = false;
		btnSairHelp.enabled = true;
	}

	public void proximaDica(){
		var canvas = GameObject.Find ("CanvasHelpProximo").GetComponent<Canvas> ();
		canvas.enabled = true;
	}

	public void sairAjuda(){
		//Time.timeScale = 1;
		var canvas = GameObject.Find ("CanvasHelp").GetComponent<Canvas> ();
		var canvasP = GameObject.Find ("CanvasHelpProximo").GetComponent<Canvas> ();
		var btnHelp = GameObject.Find ("BtnHelp").GetComponent<Button> ();
		var btnSairHelp = GameObject.Find ("BtnSairHelp").GetComponent<Button> ();
		canvas.enabled = false;
		canvasP.enabled = false;
		btnHelp.enabled = true;
		btnSairHelp.enabled = false;		
	}

	public void carregarNivel(){
		var texto = GameObject.Find ("txtCarregando").GetComponent<Text> ();
		texto.enabled = true;
	}

}
