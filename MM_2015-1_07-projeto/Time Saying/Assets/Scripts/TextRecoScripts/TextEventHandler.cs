﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class TextEventHandler : MonoBehaviour, ITextRecoEventHandler, ITrackableEventHandler
{
	private GameObject objeto;

	// Use this for initialization
	void Start () 
	{
		var trBehaviour = GetComponent<TextRecoBehaviour>();
		if (trBehaviour)
		{
			trBehaviour.RegisterTextRecoEventHandler(this);
		}
		var mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	// Update is called once per frame
	void Update () 
	{

	}

	public void OnInitialized () 
	{
		Debug.Log ("iniciou");
		//TextTracker t = (TextTracker)TrackerManager.Instance.GetTracker<TextTracker> ();
	}
	
	public void OnWordDetected(WordResult wordResult)
	{
		Debug.Log("Palavra detectada: " + wordResult.Word.StringValue + "(" + wordResult.Word.ID + ")");
		objeto = (GameObject)Instantiate (Resources.Load("frog"));
		AudioSource audioSource = objeto.AddComponent<AudioSource>();
		switch (wordResult.Word.StringValue) 
		{
			case "SAPO":				
				break;

			case "GATO":
				break;
			
			case "BOLA":
				break;
			
			default:
				break;
		}
		audioSource.clip = Resources.Load(wordResult.Word.StringValue) as AudioClip;
		audioSource.Play();
	}
	
	public void OnWordLost(Word word)
	{
		Debug.Log("Palavra perdida: " + word.StringValue + "(" + word.ID + ")");
		Destroy (objeto);
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{

		}
		else
		{

		}
	}   
}
