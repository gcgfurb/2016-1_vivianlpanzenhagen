﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic; 
using System.Text; 
using System.Threading; 
using System.IO;
using UiImage = UnityEngine.UI.Image;
#if UNITY_EDITOR
using UnityEditor;
#endif

public sealed class CarregarImagem : MonoBehaviour {

	private Sprite[] arraySprite;
	private List<string> nivel1,nivel2,nivel3, sprites;
	private string[] nomeSprite;
	//private int indexNomeSprite = 0;
	private int sizeNomeSprite = 0;
	private string spritesEscolhidas;

	#if UNITY_EDITOR
	[MenuItem ("AssetDatabase/loadAllAssetsAtPath")]
	#endif

	public void Start(){

		if (PlayerPrefs.GetString ("jogarMinhasImagens") == "sim") {
			buscarImagens ();
		} else if (PlayerPrefs.GetInt ("NivelEscolhido") == 1) {
			arraySprite = Resources.LoadAll<Sprite> ("Sprites/facil");	
		} else if (PlayerPrefs.GetInt ("NivelEscolhido") == 2) {
			arraySprite = Resources.LoadAll<Sprite> ("Sprites/medio");
		} else if (PlayerPrefs.GetInt ("NivelEscolhido") == 3) {
			arraySprite = Resources.LoadAll<Sprite> ("Sprites/dificil");	
		}

		adicionaSpriteEscolhida (); 
		buscarImagensAletorias ();   
	}

	public void buscarImagensAletorias(){
		var img = GameObject.Find ("Imagem").GetComponent<UiImage> ();
		//valor recebe um valor aleatorio no range de quantidade de sprites
		int valor = Random.Range (0, arraySprite.Length);
		//sp recebe uma sprite na posiçao aleatoria
		Sprite sp = arraySprite[valor];
		//se a quantidade de sprites escolhidas for igual a zero(Na primeira passagem...)
		if (sizeNomeSprite == 0) {
			//a imagem recebe a sprite e adiciona 1 no tamanho do array de sprites escolhidas 
			//e atribui o nome da sprite escolhida para o array que comtem o nome das sprites
			img.sprite = sp;
			sizeNomeSprite = 1;
			nomeSprite[0] = sp.name;
		} else {
			//verifica se a quantidade de sprites eh igual ao tamanho de array q contem o nome das sprites jah escolhidas
			if(arraySprite.Length == sizeNomeSprite){
				//se sim deve zerar as variaveis significando que jah foram escolhidas todas as imagens
				nomeSprite = new string[arraySprite.Length];
				img.sprite = sp;
				sizeNomeSprite = 1;
				nomeSprite[0] = sp.name;
				spritesEscolhidas = "";
				//senao chama o metodo que verifica se a imagem ja foi escolhida				 
			}else{
				//enquanto nao encontrou uma sprite que nao esta contida no array pega a proxima sprite
				while(contemSprite(sp)){
					//valor = Random.Range (0, arraySprite.Length);
					valor = (valor + 1) % arraySprite.Length;
					sp = arraySprite[valor];
				}
				//a imagem recebe a sprite 
				img.sprite = sp;
				nomeSprite[sizeNomeSprite] = sp.name;
				sizeNomeSprite++;
			}
		}
		if (spritesEscolhidas.Equals ("")) {
			spritesEscolhidas = sp.name;
		} else {
			spritesEscolhidas = spritesEscolhidas + ";" + sp.name;
		}
		PlayerPrefs.SetString ("SpriteEscolhida", spritesEscolhidas);
	}
	

	private void adicionaSpriteEscolhida(){
		//cria um array de string com o tamanho da quantidade de sprites 
		nomeSprite = new string[arraySprite.Length];
		spritesEscolhidas = PlayerPrefs.GetString ("SpriteEscolhida");
		//cria array de sprites e separa com ;
		string[] sprites = spritesEscolhidas.Split (';');
		//para a quantidade de sprites  verifica se e diferente de vazio
		//e sizeNomeSprite e o tamanho do array de sprites escolhidas recebe 0 
		for(int i = 0; i < sprites.Length;i++){
			if(sprites[i].Equals("")){
				sizeNomeSprite = 0;
				return;
			}
			//o array q contem o 
			nomeSprite[i] = sprites[i];
		}
		sizeNomeSprite = sprites.Length;
	}

	private bool contemSprite(Sprite sp){
		for (int i = 0; i < nomeSprite.Length; i ++) {
			if (sp.name.Equals (nomeSprite [i])) {
				return true;
			}
		}
		return false;
	}

	public void buscarImagens(){
		//string myDir = "C:\\Users\\Vivian\\Documents";
		string myDir = "/storage/emulated/0/Download/VISEDU";
		DirectoryInfo dir = new DirectoryInfo (myDir);
		nivel1 = new List<string>();
		nivel2 = new List<string>();
		nivel3 = new List<string>();
		
		List<System.IO.FileInfo> info = new List<System.IO.FileInfo> ();
		info.AddRange (dir.GetFiles ("*.png", System.IO.SearchOption.TopDirectoryOnly));
		info.AddRange (dir.GetFiles ("*.jpg", System.IO.SearchOption.TopDirectoryOnly));
		if (info != null) {
			string[] paths = new string[info.Count];
			for (int i = 0; i < info.Count; i++) {
				paths [i] = info [i].FullName.ToString ();
				string nomeImagem = Path.GetFileNameWithoutExtension (paths [i]);

				if (nomeImagem.Contains ("F_")) {
					nivel1.Add(paths[i]);
				} else if (nomeImagem.Contains ("M_")) {
					nivel2.Add(paths[i]);
				} else if (nomeImagem.Contains ("D_")) {
					nivel3.Add(paths[i]);
				}
			}
		}
		if (PlayerPrefs.GetInt ("NivelEscolhido") == 1) {
			sprites = nivel1;
		} else if (PlayerPrefs.GetInt ("NivelEscolhido") == 2) {
			sprites = nivel2;
		} else if (PlayerPrefs.GetInt ("NivelEscolhido") == 3) {
			sprites = nivel3;
		}

		arraySprite = new Sprite[sprites.Count];

		for (int i = 0; i < sprites.Count; i++) {
			byte[] data = File.ReadAllBytes (sprites.ElementAt(i));
			Texture2D texture = new Texture2D (270, 186, TextureFormat.ARGB32, false);
			texture.LoadImage (data);
			texture.name = Path.GetFileNameWithoutExtension (sprites.ElementAt(i));
			Sprite spt = Sprite.Create (texture,new Rect(0,0,texture.width, texture.height),Vector2.zero);
			spt.name = texture.name.Substring(2);
			Debug.Log ("Sprite Texture = " + spt.name);
			arraySprite[i] = spt;
			}
	}		
		

	//metodo que altera o arquivo AdditionalWords.lst
	public void addAdditionalWords(string nomeImagem){
		StreamWriter x;
		string CaminhoNome = "Assets\\StreamingAssets\\QCAR\\AdditionalWords.lst";
		string[] lines = System.IO.File.ReadAllLines (CaminhoNome);

		var teste2 = GameObject.Find ("teste2").GetComponent<Text> ();
		teste2.text = " Encontrou Arquivo: "+lines.Length;

		x = File.AppendText (CaminhoNome);
		string nm = "";
		bool contem = false;
		foreach (string line in lines) {
			Debug.Log ("Linha:     " + line);
			nm = nomeImagem.ToUpper ();
			if (line.Contains (nm)) {
				contem = true;
			}
		}
		if (contem == false){
			x.WriteLine(nm);
		}
		x.Close ();
	}

	public Sprite getSpriteAleatoria(){
		int valor = Random.Range (0, arraySprite.Length);
    	Sprite sp = arraySprite[valor];
		PlayerPrefs.SetString ("SpriteEscolhida",sp.name);
		Debug.Log ("testeeeeeeeeee: "+sp.name);
		return sp;
	}

}

