﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptBotoes : MonoBehaviour {

	public void jogarImagensPadrao(){
		PlayerPrefs.SetString("jogarMinhasImagens","nao");
		Application.LoadLevel ("CenaNivel");
	}

	public void jogarMinhasImagens(){
		PlayerPrefs.SetString("jogarMinhasImagens","sim");
		Application.LoadLevel ("CenaNivel");
	}

	public void imprimir(){
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/palavras/letras.pdf");	
	}

	public void responderQustionario(){
		Application.OpenURL ("http://docs.google.com/forms/d/1sedYTH5hN1h7HH5L7_WVQU2BXB325uM0Cq_JwsH3x8g/prefill");
	}

}