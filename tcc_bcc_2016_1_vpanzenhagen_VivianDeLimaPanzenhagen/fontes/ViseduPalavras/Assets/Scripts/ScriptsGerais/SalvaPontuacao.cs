﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SalvaPontuacao : MonoBehaviour {

	void Start () {

		var nota = GameObject.Find("txtNota").GetComponent<Text>();	
		int pontos = 0;
		if (PlayerPrefs.HasKey("SalvaPonto")== true){ 
			pontos = PlayerPrefs.GetInt("SalvaPonto") ; 
		}	
		nota.text = ""+pontos;
	}
}
