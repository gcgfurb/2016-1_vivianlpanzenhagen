﻿using UnityEngine;
using System.Collections;

public class LifebarScript : MonoBehaviour {

	public Transform fill;

	public void UpdateBar(float life){
		fill.localScale = new Vector3 (life, 1f, 1f);
	}
}
