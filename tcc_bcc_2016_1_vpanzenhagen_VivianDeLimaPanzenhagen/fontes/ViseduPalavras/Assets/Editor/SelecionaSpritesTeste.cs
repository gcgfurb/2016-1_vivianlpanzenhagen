﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

using System.Collections.Generic; 
using System.Text; 
using System.Threading; 
using System.IO;

using UnityEditor;


public sealed class SelecionaSpritesTeste : MonoBehaviour {

	public Sprite[] arraySprite;
	public Sprite[] arraySpriteEscolhido;
	private Sprite refazerSprite;
	private int qtdSpriteEsquilhido;


	[MenuItem ("AssetDatabase/loadAllAssetsAtPath")]

	public void Start(){

		if(PlayerPrefs.GetInt("NivelEscolhido")== 1) {
			carregaArraySprite("/Resources/Sprites/facil");
		}else if(PlayerPrefs.GetInt("NivelEscolhido")== 2) {
			carregaArraySprite("/Resources/Sprites/medio");
		}else if(PlayerPrefs.GetInt("NivelEscolhido")== 3) {
			carregaArraySprite("/Resources/Sprites/dificil");
		}

		arraySpriteEscolhido = new Sprite[arraySprite.Length];
		Debug.Log("Dentro do array de sprite "+arraySprite.Length);
		qtdSpriteEsquilhido = 0;


		StreamWriter x;
		string CaminhoNome = "Assets\\StreamingAssets\\QCAR\\AdditionalWords.lst";
		x = File.AppendText(CaminhoNome);
		string nm = "";
		for(int i=0; i < arraySprite.Length; i++){ 
			nm = arraySprite[i].name.ToUpper();
				x.WriteLine(nm);		
		}
		x.Close();	
	}


	private void carregaArraySprite(string myDir){

		DirectoryInfo dir = new DirectoryInfo(Application.dataPath + myDir);
		if(!dir.Exists){
			Debug.Log ("no folder!");
			return;
		}
		List<System.IO.FileInfo> info = new List<System.IO.FileInfo>();
		//load supported file types here
		info.AddRange(dir.GetFiles("*.png", System.IO.SearchOption.TopDirectoryOnly));
		info.AddRange(dir.GetFiles("*.gif", System.IO.SearchOption.TopDirectoryOnly));
		info.AddRange(dir.GetFiles("*.jpg", System.IO.SearchOption.TopDirectoryOnly));
		
		if(info != null){
			arraySprite = new Sprite[info.Count];
			string[] paths = new string[info.Count];
			for(int i = 0 ; i < info.Count; i++){
				paths[i] = info[i].FullName.ToString();
				Debug.Log ("got a proper path: " + paths[i]);

				Sprite sprite = AssetDatabase.LoadAssetAtPath("Assets" + myDir + "/" + Path.GetFileName(paths[i]), typeof(Sprite)) as Sprite;

				Debug.Log ("Sprite: " + sprite.name + " " + sprite.textureRect );
				arraySprite[i]=sprite;

				//access the sprite's texture here at sprite.texture
			}
		}
	}


	public Sprite getSpriteAleatoria(){
		int valor = Random.Range (0, arraySprite.Length);
    	Sprite sp = arraySprite[valor];
		/*
		for (int j=0; j < arraySpriteEscolhido.Length; j++) {
			string nome = sp.name;
			if(arraySpriteEscolhido[j] != null){
				string nome2 = arraySpriteEscolhido[j].name;
				if (nome.Equals(nome2)){
					return null;
				}

			}
			
		}
		arraySpriteEscolhido[qtdSpriteEsquilhido] = sp;
		qtdSpriteEsquilhido++;
*/	
		PlayerPrefs.SetString ("SpriteEscolhida",sp.name);
		Debug.Log ("testeeeeeeeeee: "+sp.name);
		var teste = GameObject.Find ("teste").GetComponent<Text> ();
		teste.text = "Imagem:" + sp;

		return sp;
	}

}

